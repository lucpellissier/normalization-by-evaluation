%% This is Luc's book class. It is merely suited for his own taste.
%%
%% Copyright 2013, Luc Pellissier
%% email : luc.pellissier@crans.org
%%
%% 
%% Unlimited copying and redistribution of this file are permitted as
%% long as this file is not modified.  Modifications, and distribution
%% of modified versions, are permitted, but only if the resulting file
%% is renamed.
%% 

%% Version history :
%%		0.1 2013/05/04 First release
%%		0.2 2013/09/24 Based on article and not on amsart
\NeedsTeXFormat{LaTeX2e}% LaTeX 2.09 can't be used (nor non-LaTeX)
               [1995/06/01]% LaTeX date must be June 1995 or later
\ProvidesClass{lucart}[2013/09/24 v0.2]

\LoadClass[a4paper,article,10pt]{memoir}
\DeclareMathSizes{10}{9}{6}{5}

\counterwithout{section}{chapter}

\nouppercaseheads
\pagestyle{headings}

%% Simulate amsart

\setmarginnotes{10pt}{50pt}{5pt}
\setsecheadstyle{\scshape\centering\large}
\setsubsecheadstyle{\itshape\bfseries\raggedright\noindent}
% \setaftersubsecskip{-1ex}
\setsubsecindent{0ex}
% \setsecnumdepth{section}
\setlength{\parskip}{0.1em}
% \chapterstyle{ell}
% \counterwithout{section}{chapter}

%% Gestion des marges

\RequirePackage{geometry}
\geometry{
	left=3cm,
	textwidth=15cm,
	top=3cm,
	bottom=3cm,
	a4paper}
\ProcessOptions\relax

%% Mathématiques

\RequirePackage{amsmath}
\RequirePackage{unicode-math}
\RequirePackage{amsthm}
\RequirePackage{wasysym}


%% Gestion des langues

\RequirePackage{polyglossia}

\setdefaultlanguage{english}
\unimathsetup{math-style=french}
\ProcessOptions\relax

%% Polices

% \usepackage{luatextra}
\defaultfontfeatures{Ligatures=TeX}
% \setmainfont[Ligatures=TeX]{Adobe Garamond Pro}
% \setsansfont[Scale=MatchLowercase,Ligatures=TeX]{Helvetica Neue}
% \setmonofont[Scale=MatchLowercase]{Monaco}
% \setmathfont{Asana-Math.otf}
%% Bibtex

\RequirePackage{csquotes}
\RequirePackage
    [
    style=authoryear-comp,
    labelnumber,
    % backref=true,
    % dashed=false,
    isbn=false,
    doi=false,
    url=false,
    % mergedate=false,
    defernumbers = true,
    backend=biber,
    maxbibnames=99,
    ]
    {biblatex}
\DefineBibliographyStrings{english}{%
  backrefpage  = {cited p.}, % for single page number
  backrefpages = {cited pp.} % for multiple page numbers
}
\DeclareNameAlias{sortname}{first-last}
\AtEveryBibitem{
  \clearlist{language}
}
\DefineBibliographyExtras{french}{%
  \renewcommand{\mkbibnamelast}[1]{{\hyphenrules{nohyphenation}#1}}%
}
\DeclareFieldFormat[article,periodical]{volume}{\mkbibbold{#1}}

\firmlists

%% Miscélannées

\RequirePackage{environ}
\RequirePackage{xargs}
\RequirePackage[protrusion=true]{microtype}
\RequirePackage{url}
%% \RequirePackage{subfigure}
\RequirePackage{lettrine}
\RequirePackage{graphics}
\RequirePackage{color}
\RequirePackage{titletoc}
% \RequirePackage{imakeidx}
\RequirePackage{epigraph}
\RequirePackage{cmll}
\RequirePackage{stmaryrd}

\setlength{\epigraphwidth}{0.3\pagewidth}


%% Hyperref

\RequirePackage{hyperref}

\hypersetup{
  bookmarksnumbered=true,     
  bookmarksopen=true,         
  bookmarksopenlevel=1,           
  pdfstartview=Fit,           
  pdfpagemode=UseOutlines,
  pdfpagelayout=TwoPageRight
}

\RequirePackage[noabbrev]{cleveref}

\renewcommand{\qedsymbol}{\smiley}

\renewcommand{\arraystretch}{1.1}
\newcolumntype{C}[1]{>{\centering\arraybackslash}m{#1}}


\newtheorem{example}{Example}
\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}

\theoremstyle{definition}
\newtheorem{definition}{Definition}

\RequirePackage{tikz-cd}
\tikzcdset{ampersand replacement = \&}