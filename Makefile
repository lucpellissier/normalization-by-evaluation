all:
	emacs --script export.el lectures.org
	latexmk -lualatex main

clean:
	rm lectures.tex
	latexmk -C main
	rm main.bbl
	rm main.run.xml
