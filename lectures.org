#+TITLE:  Normalization-by-evaluation and related techniques
#+AUTHOR: Luc Pellissier
#+LANGUAGE:  en


* Motivations
** Definability
The notion of definability originated in \parencite{1980:Plotkin} as a way of
establishing a completeness result. Indeed, consider a model of the simply-typed
\(\lambda\)-calculus, that is a family of sets \((D_{\sigma})_{\sigma}\) indexed
by the types such that for all types \(\sigma\) and \(\tau\), \(D_{\sigma \to
\tau}\) is the set of functions from \(D_{\sigma}\) to \(D_{\tau}\). Not all
elements of such a set is the interpretation of a \(\lambda\)-term. Such
elements are said /definable/.

\textcite{1980:Plotkin} characterizes definable terms for the full type
hierarchy of the simply typed \(\lambda\)-calculus. He does so by, given an
arity \(\kappa\) (an ordinal), defining a family of relations \(R_{\sigma}
\subseteq D_{\sigma}^{\kappa}\) that relates indistinguishable elements. Given
any base relation \(R_{\iota} \subseteq D^{\kappa}\), we want to define
\begin{align*}
  R_{\sigma \to \tau}(f^{(\kappa)}) \EquivDef
  (\forall d \in D_{\sigma}, R_{\sigma}(d^{(\kappa)}) \implies
  R_{\tau}(f(d)^{(\kappa)}))
\end{align*}
but this definition fails to provide the desired theorem above
rank 2. Nonetheless, interpreting the implication in the definition of a
relation \(R_{\sigma \to \tau}\) intuitionistically does the job: the relation
takes a further argument \(w \in \worlds\), a set of worlds, so \(R_{\sigma}
\subseteq D_{\sigma}^{\kappa} \times \worlds\) and
\begin{align*}
  R_{\sigma \to \tau}(f^{(\kappa)},w) \EquivDef
  \left(\forall d \in D_{\sigma},
  \left(\forall w' \geqslant w, R_{\sigma}(d^{(\kappa)},w')\right)
  \implies
  R_{\tau}(f(d)^{(\kappa)}),w)\right)
\end{align*}
Such a relation is called a /Kripke logical relation/ and we say that an element
\(d \in D_{\sigma}\) satisfies \(R_{\sigma}\) if, for all worlds \(w\in
\worlds\), \((d^{\kappa}, w) \in R_{\sigma}\).

\begin{theorem}[Plotkin]
  Suppose \(D\) infinite.

  An element \(d\in D\) is \(\lambda\)-definable if and only if it satisfies
  every Kripke logical relation \(R \subseteq D^3 \times \worlds\).
\end{theorem}

The completeness part extracts a \(\lambda\)-term out of an element that
satisfies \(R_{\sigma}\). The proof is parametrized by a syntactical context
\(\Gamma\) (represented by an element of \(\worlds\) and a valuation on
\(\Gamma\).

This can be generalized in many directions :
- consider a \(\lambda\)-calculus with arbitrary constants at any type (so as to
  do realizability);
- consider a \(\lambda\)-calculus with more constructions (say, sums, products,
  \textellipsis);
- consider models in arbitrary categories, and not just the category of sets

\textcite{1999:FioreSimpson} prove a definability result for a \(\lambda\)-calculus:
- with any number of constants;
  
  The way to do so is by allowing relations to have variable arity: instead of
  having just a relation included in \(D^{k}\), we will take relations in the
  union of the powers of \(D\).
- with sums, products and their respective units;
  
  This necessitates to topologize the worlds in order to be able to do something
  with the sum.
- interpreted in any stable bicartesian closed category.
** Normalization by Evaluation
* Sheaves and pre-sheaves
Let \(\catC\) be a small category. The /category of presheaves over \(\catC\)/ is
the category \(\Presheafs{\catC} = [\op{\catC},\Set]\), whose objects are functors
\(\op{\catC} \to \Set\) and morphisms are their natural transformations. It is
bicartesian closed (and co-complete).

#+ATTR_LATEX: :options [Grothendieck topology, site]
#+BEGIN_definition
A /Grothendieck topology/ \(K\) over a category \(\catC\) is a collection of
families of /covers/ \(K(c)\) for every object \(c:\catC\) such that
\begin{align*}
  \forall I \in K(c), \forall f \in I, \exists d:\catC, f : d \to c
\end{align*}
and, for every object \(c:\catC\):
- Identity :: \(\{\id_c :c \to c\} \in K(c)\);
- Stability :: for every family \(I \in K(c)\) and morphism \(\psi : d\to c\),
  there exists a family \(J \in K(d)\) such that for each \(\gamma \in J\),
  there exists a \(\varphi \in I\) such that \(\psi \circ \gamma\) factors
  through \(\varphi\);
- Transitivity :: if \(I \in K(c)\) and for every \(\varphi : d_{\varphi} \to
  c \in I\), \(J_{\varphi} \in K(d_{\varphi})\), then
  \begin{align*}
    \{ \varphi \circ \gamma \mid \varphi \in I, \gamma \in J_{\varphi} \} \in K(c) 
  \end{align*}

A /site/ is a small category together with a Grothendieck topology over it.
#+END_definition

The prototypical example of a site is the site of opens of a topological space,
with inclusions as arrows. In this case, the three conditions become:

\begin{tikzpicture}
  
\end{tikzpicture}

#+ATTR_LATEX: :options [sheaf]
#+BEGIN_definition
Let \(\catC\) be a small category and \(K\) a Grothendieck topology over it.

Let \(X : \op{\catC} \to \Set\) be a pre-sheaf over \(\catC\).

Let \(I \in K\) be a covering family. A /\((I,X)\)-compatible family of elements/
is a family
\begin{align*}
  (s_{\gamma})_{\gamma \in I} \in \prod_{\gamma \in I} X(c_{\gamma})
\end{align*}  
where we write \(c_{\gamma}\) the domain of each \(\gamma \in I\) and \(c\)
their common co-domain, such that for all \(\psi,\gamma \in I\) and morphisms
\(f,g\) such that,
\begin{align*}
  \begin{tikzcd}[ampersand replacement = \&]
    \&  d\ar[ld,"f"] \ar[rd,"g"]\&\\
    U_{\gamma} \ar[rd,"\gamma"]\&\& U_{\varphi} \ar[ld,"\psi"]\\
    \& c
  \end{tikzcd}
\end{align*}
implies \(X(f)(s_{\gamma}) = X(g)(s_{\varphi})\)

\(X : \op{\catC} \to \Set\) is a /sheaf/ if, for every covering family \(I \in
K\), and for every \(I,X\)-compatible family of elements \((s_{\gamma})_{\gamma
\in I} \in \prod_{\gamma \in I} X(c_{\gamma})\), there is a \emph{unique} \(s\in
X(c)\) such that \(X(\gamma)(s) = s_{\gamma}\).
#+END_definition

The category of sheaves over a site \((\catC,K)\), noted by
\(\Sheaves(\catC,K)\) is the full subcategory of \(\Presheafs{\catC}\) whose
objects are sheaves. The embedding \(\Sheaves(\catC,K) \hookrightarrow
\Presheafs{\catC}\) has a left-exact left-adjoint
\begin{align*}
  \Sheafification : \Presheafs{\catC} \to \Sheaves(\catC,K)
\end{align*}
* Grothendieck relations
Let \(\worlds\) and \(\semantics\) be small categories and \(a : \worlds \to
\semantics\) a functor.

\(a\) induces a functor \(a \ast : \semantics \to \Presheafs{\worlds}\) given by:
\begin{align*}
  \forall A : \semantics, (a \ast A)(\bullet) = \semantics(a\bullet, A)
\end{align*}

#+ATTR_LATEX: :options [Grothendieck relation]
#+BEGIN_definition
Let \(\worlds, K\) be a site and \(a : \worlds \to \semantics\) a functor.

A /\((\worlds, K)\)-Grothendieck relation of arity \(a\) over
\(D : \semantics\)/ is a sub-sheaf of the sheafification
\(\Sheafification(a\ast D)\) of the presheaf \(a \ast D\).
#+END_definition

Let us unpack this definition. First of all, it is well-known that, given a site
\((\catC,K\) and \(P : \op{\catC} \to \Set\), sub-sheaves of \(\Sheafification
P\) are in natural bijection with sub-presheaves that are /\(K\)-closed/: for
every cover \(J \in K(w)\) and all \(x\in P(w)\), we have
\begin{align*}
  \left(\forall (\gamma : w_{\gamma} \to w) \in J, P(\gamma)(x)
    \in R(w_{\gamma})
  \right)
  \implies
  x \in R(w)
\end{align*}

Hence, a \((\worlds, K)\)-Grothendieck relation \(R\) of arity \(a\) over \(A :
\semantics\) is a closed sub-presheaf of \(a \ast A\). We can reformulate
further this fact as:
#+ATTR_LATEX: :options [characterization of Grothendieck relations]
#+BEGIN_proposition
Let \(R\) be a family of sets indexed over \(\worlds\).

\(R\) is a \((\worlds, K)\)-Grothendieck relation of arity \(a\) over \(A :
\semantics\) if and only if:
- variable arity \(a\) over \(A\) :: for every \(w : \worlds\), \(R(w) \subseteq
  \semantics(aw, A)\)
- monotonicity :: for every \(\psi : w \to v : : \worlds\) and \(x : a(v) \to
  A : : \semantics\), if \(x \in R(v)\) then
  \begin{align*}
    \left(
    \begin{tikzcd} a(w) \ar[r,"a(\psi)"] \& a(v) \ar[r,"x"] \& A \end{tikzcd}
    \right)
    \in R(w)
  \end{align*}
- continuity :: for every cover \(J \in K(w)\) and all \(x : a(w) \to A : :
  \semantics\), if for every \((\gamma : w_{\gamma} \to w) \in J\), \(x \circ (a
  (\gamma)) \in R(w_{\gamma})\) then \(x \in R(w)\).
#+END_proposition

We also define a category of \((\worlds, K)\)-Grothendieck relations of arity
\(a\) in a category \(\semantics\):

#+ATTR_LATEX: :options [sconing category]
#+BEGIN_definition
Let \(\worlds, K\) be a site and \(a : \worlds \to \semantics\) a functor.

The category \(\Scone(\worlds,K;a)\) of \((\worlds, K)\)-Grothendieck
relations of arity \(a\) in \(\semantics\) has:
 - objects :: pairs \((A,R)\) of an object \(A:S\) and a
   \((\worlds, K)\)-Grothendieck relation \(R\) of arity \(a\) over
   \(A\);
 - morphisms :: \((A,R) \to (B,S)\) are arrows \(f:A \to B : : \semantics\)
   such that, for all \(x:a(w) \to A\),
   \begin{align*}
     x \in R(w) \implies f \circ x \in S(w).
   \end{align*}
#+END_definition

#+ATTR_LATEX: :options [bicartesian closed structure]
#+BEGIN_proposition
Let \(\worlds, K\) be a site, \(\semantics\) bicartesian closed and \(a :
\worlds \to \semantics\) a functor.

\(\Scone(\worlds,K;a)\) is bicartesian closed and the forgetful functor
\(\Scone(\worlds,K;a) \to \semantics\) is faithful and preserves and reflects
the bicartesian closed structure.
#+END_proposition

#+BEGIN_proof
Let us review the different elements of the structure. Let \((A,R)\) and
\((B,Q)\) be two objects in \(\Scone(\worlds,K;a)\).
- products ::
  We define \(R \circledast Q\) by, for \(w:\worlds\),
  \begin{align*}
    \left(
      \begin{tikzcd}
        a(x) \ar[r, "x"] \& A \times B
      \end{tikzcd}
      \right)
      \in (R \circledast Q)(w)
    \EquivDef \left\{
                \begin{array}{l}
                  \left(
                  \begin{tikzcd}
                    a(x) \ar[r,"x"] \& A \times B \ar[r,"\pi_1"] \& A
                  \end{tikzcd}
                  \right)
                  \in R(w)\\
                  \left(
                  \begin{tikzcd}
                    a(x) \ar[r,"x"] \& A \times B \ar[r,"\pi_2"] \& B
                  \end{tikzcd}
                  \right)
                  \in Q(w)
                \end{array}
    \right.
  \end{align*}
  \(R \circledast Q\) is a Grothendieck relation over \(A\times B\). Indeed:
  + by definition, \((R \circledast Q)(w) \subseteq \semantics(aw, A \times
    B)\);
  + let \(\psi : w \to v : : \worlds\) and \(x : a(v) \to A \times B : :
    \semantics\). Suppose \(x \in (R\circledast Q)(v)\). By definition of
    \(R\circledast Q\), we have that \((\pi_{1} \circ x) \in R(v)\) which, as
    \(R\) is a Grothendieck relation, implies \((\pi_{1} \circ x \circ a(\psi))
    \in R(w)\).

    Similarly, \((\pi_{2} \circ x \circ a(\psi)) \in Q(w)\), hence, by
    definition of \(R \circledast Q\), \((x \circ a(\psi)) \in (R \circledast
    Q)(w)\).
  + let \(J \in K(w)\) be a cover and \(x : a(w) \to A : : \semantics\). Suppose
    for every \((\gamma : w_{\gamma} \to w) \in J\), \(x \circ (a (\gamma)) \in
    (R \circledast Q)(w_{\gamma})\).
    
    By definition of \(R \circledast Q\), we have that \(\pi_{1} \circ x \circ
    (a (\gamma)) \in R (w_{\gamma})\) and \(\pi_{2} \circ x \circ (a (\gamma))
    \in Q (w_{\gamma})\), and as \(R\) and \(Q\) are Grothendieck relations,
    \(\pi_{1} \circ x \in R(w)\) and \(\pi_{2} \circ x \in Q(w)\), which, by
    definition of \(R \circledast Q\), implies that \(x \in (R \circledast
    Q)(w))\).
- sums :: 
  We define \(R \oplus Q\) by, for \(w:\worlds\),
  \begin{align*}
    &\left(
      \begin{tikzcd}
        a(w) \ar[r,"x"] \& A_1 \sqcup A_2
      \end{tikzcd}
      \right) \in (R_1 \oplus R_2)(w)\\
    \EquivDef
    &\exists I \in K(w), \forall (\gamma : w_{\gamma} \to w) \in I,
      \exists k \in \{1,2\}, \exists \left(
      \begin{tikzcd}
        a(w_{\gamma}) \ar[r,"x^{\gamma}_k"] \& A_k
      \end{tikzcd}
      \right)
      \in R_k(w_{\gamma}),\\
    &\iota_k \circ x^{\gamma}_k = x \circ a(\gamma)
  \end{align*}
  \(R_{1} \oplus R_{2}\) is is a Grothendieck relation over \(A_{1} \sqcup
  A_{2}\). Indeed:
  + by definition, \((R_{1} \oplus R_{2})(w) \subseteq \semantics(aw, A_{1}
    \sqcup A_{2})\);
  + let \(\psi : w \to v : : \worlds\) and \(x : a(v) \to A_{1} \sqcup A_{2} : :
    \semantics\). Suppose \(x \in R_{1} \oplus R_{2}(v)\). We are going to show
    that \((x \circ a(\psi)) \in R_{1} \oplus R_{2}(w)\).

    By definition of \(R_{1} \oplus R_{2}\), let \(I \in K(v)\) be the cover
    satisfying the definition. By stability of the Grothendieck topology, let
    \(J \in K(w)\) pulled from \(I\) along \(\psi\).

    Let \((\gamma : w_{\gamma} \to w) \in J\). Let \((\phi : v_{\phi} \to v) \in
    I\) be such that
    \begin{align*}
      \begin{tikzcd}
        \& w_{\gamma} \ar[ld, "\gamma"] \ar[rd,"\theta"] \&\\
        w \ar[rd, "\psi"]\& \& v_{\phi} \ar[ld, "\phi"]\\
        \& v
      \end{tikzcd}
    \end{align*}
    Let \(k \in \{1,2\}\) and
    \((x^{\phi}_{k} : v_{\psi} \to A_{k}) \in R_k(v_{\phi})\) be such that
    \(\iota_k \circ x^{\phi}_k = x \circ a(\phi)\). So:
    \begin{align*}
      \begin{tikzcd}
        \& a(w_{\gamma}) \ar[ld, "a(\gamma)"] \ar[rd,"a(\theta)"] \&\\
        a(w) \ar[rd, "a(\psi)"]\& \& a(v_{\phi}) \ar[ld, "a(\phi)"] \ar[rd,"x_k^{\phi}"]\\
        \& a(v) \ar[rd,"x"] \& \& A_k \ar[ld,"\iota_k"]\\
        \& \& A_1 \sqcup A_2
      \end{tikzcd}
    \end{align*}
    and as \(R_{k}\) is a Grothendieck relation, \((x_{k}^{\phi} \circ
    a(\theta)) \in R_{k}\).  Hence, by definition of \(R_{1} \oplus R_{2}\),
    \((x \circ a(\psi)) \in R_{1} \oplus R_{2}(w)\).
  + let \(J \in K(w)\) be a cover and \(x : a(w) \to A : :
    \semantics\). Suppose that for every \((\gamma : w_{\gamma} \to w) \in J\),
    \(x \circ a(\gamma) \in (R_{1} \oplus R_{2})(w_{\gamma})\).

    For \((\gamma : w_{\gamma} \to w) \in J\), let \(I_{\gamma} \in
    K(w_{\gamma})\) be satisfying the covering condition on \(\gamma\) in the
    definition of \(R_{1} \oplus R_{2}\):
    \begin{align*}
      \forall (\psi : w_{\psi} \to w_{\gamma}) \in I_{\gamma},
      \exists k \in \{1,2\}, \exists \left(
      \begin{tikzcd}
        a(w_{\psi}) \ar[r,"x^{\psi;\gamma}_k"] \& A_k
      \end{tikzcd}
      \right)
      \in R_k(w_{\psi}),
      \iota_k \circ x^{\psi;\gamma}_k = x \circ a(\gamma) \circ a(\psi)
    \end{align*}
    Consider the collection \(K = \{ \gamma \circ \varphi \mid \gamma \in J,
    \varphi \in I_{\gamma} \}\) which is a covering of \(w\), by
    transitivity. So, \(x\in (R_{1}\oplus R_{2})(w)\), with \(K\) as the
    covering witness.
- exponentials :: 
#+END_proof

Which is just a complicated way to say that for every term \(\vdash t : \tau\),
the interpretation
\begin{align*}
  \Semantics{\vdash t : \tau} : 1 \to \Semantics{\tau}
\end{align*}
satisfies \(R_{\tau}\), for \(R\) a Grothendieck relation such that all
constants are satisfied.
* The theorem
* References
\printbibliography[heading=none]
* Configuration :noexport:
# Local variables:
# ispell-local-dictionary: "en"
# End:
